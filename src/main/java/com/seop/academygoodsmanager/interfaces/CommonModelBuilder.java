package com.seop.academygoodsmanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
