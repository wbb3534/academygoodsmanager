package com.seop.academygoodsmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GoodsName {
    MONITOR("모니터"),
    KEYBOARD("키보드"),
    MOUSE("마우스"),
    CHAIR("의자"),
    DESKTOP("본체");
    private final String goodsName;
}
