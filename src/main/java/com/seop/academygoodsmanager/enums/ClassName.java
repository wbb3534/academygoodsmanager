package com.seop.academygoodsmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ClassName {
    ONE_CLASS("1강의실"),
    TWO_CLASS("2강의실"),
    THREE_CLASS("3강의실"),
    FOUR_CLASS("4강의실"),
    FIVE_CLASS("5강의실"),
    SIX_CLASS("6강의실"),
    WAREHOUSE("창고")
    ;
    private final String name;
}
