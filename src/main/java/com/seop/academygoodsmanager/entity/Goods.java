package com.seop.academygoodsmanager.entity;

import com.seop.academygoodsmanager.enums.GoodsName;
import com.seop.academygoodsmanager.interfaces.CommonModelBuilder;
import com.seop.academygoodsmanager.model.GoodsRequest;
import com.seop.academygoodsmanager.model.ManagerNumUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Goods {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private GoodsName goodsName;
    @Column(nullable = false)
    private Double goodsPrice;
    @Column(nullable = false)
    private Integer managerNum;
    @Column(nullable = false)
    private LocalDate receiveDate;
    @Column(nullable = false)
    private String receivePeople;

    public void putManagerNum(ManagerNumUpdateRequest request) {
        this.managerNum = request.getManagerNum();
    }
    private Goods(GoodsBuilder builder) {
        this.goodsName = builder.goodsName;
        this.goodsPrice = builder.goodsPrice;
        this.managerNum = builder.managerNum;
        this.receiveDate = builder.receiveDate;
        this.receivePeople = builder.receivePeople;
    }
    public static class GoodsBuilder implements CommonModelBuilder<Goods> {
        private final GoodsName goodsName;
        private final Double goodsPrice;
        private final Integer managerNum;
        private final LocalDate receiveDate;
        private final String receivePeople;

        public GoodsBuilder(GoodsRequest request) {
            this.goodsName = request.getGoodsName();
            this.goodsPrice = request.getGoodsPrice();
            this.managerNum = request.getManagerNum();
            this.receiveDate = LocalDate.now();
            this.receivePeople = request.getReceivePeople();
        }
        @Override
        public Goods build() {
            return new Goods(this);
        }
    }
}
