package com.seop.academygoodsmanager.entity;

import com.seop.academygoodsmanager.enums.ClassName;
import com.seop.academygoodsmanager.interfaces.CommonModelBuilder;
import com.seop.academygoodsmanager.model.ManagerNameUpdateRequest;
import com.seop.academygoodsmanager.model.ManagerRequest;
import com.seop.academygoodsmanager.model.PlaceIsUsedUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Manager {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long managerId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "goodsId", nullable = false)
    private Goods goods;
    @Column(nullable = false, length = 20)
    private String managerFirst;
    @Column(nullable = false, length = 20)
    private String managerSecond;
    @Column(length = 20)
    @Enumerated(value = EnumType.STRING)
    private ClassName className;
    @Column(nullable = false)
    private Boolean isUsed;

    public void putManagerName(ManagerNameUpdateRequest request) {
        this.managerFirst = request.getManagerFirst();
        this.managerSecond = request.getManagerSecond();
    }
    public void putPlaceIsUsed(PlaceIsUsedUpdateRequest request) {
        this.className = request.getClassName();
        this.isUsed = request.getIsUsed();
    }
    private Manager(ManagerBuilder builder) {
        this.goods = builder.goods;
        this.managerFirst = builder.managerFirst;
        this.managerSecond = builder.managerSecond;
        this.className = builder.className;
        this.isUsed = builder.isUsed;
    }
    public static class ManagerBuilder implements CommonModelBuilder<Manager> {
        private final Goods goods;
        private final String managerFirst;
        private final String managerSecond;
        private final ClassName className;
        private final Boolean isUsed;

        public ManagerBuilder(Goods goods, ManagerRequest request) {
            this.goods = goods;
            this.managerFirst = request.getManagerFirst();
            this.managerSecond = request.getManagerSecond();
            this.className = request.getClassName();
            this.isUsed = request.getIsUsed();
        }
        @Override
        public Manager build() {
            return new Manager(this);
        }
    }
}
