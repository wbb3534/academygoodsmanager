package com.seop.academygoodsmanager.model;

import com.seop.academygoodsmanager.enums.ClassName;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PlaceIsUsedUpdateRequest {
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private ClassName className;
    @NotNull
    private Boolean isUsed;
}
