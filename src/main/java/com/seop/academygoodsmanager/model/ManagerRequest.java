package com.seop.academygoodsmanager.model;

import com.seop.academygoodsmanager.enums.ClassName;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ManagerRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String managerFirst;
    @NotNull
    @Length(min = 2, max = 20)
    private String managerSecond;
    @Enumerated(value = EnumType.STRING)
    private ClassName className;
    @NotNull
    private Boolean isUsed;
}
