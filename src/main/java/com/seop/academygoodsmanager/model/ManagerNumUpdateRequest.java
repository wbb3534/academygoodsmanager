package com.seop.academygoodsmanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ManagerNumUpdateRequest {
    @NotNull
    private Integer managerNum;
}
