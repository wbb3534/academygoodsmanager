package com.seop.academygoodsmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ManagerNameUpdateRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String managerFirst;
    @NotNull
    @Length(min = 2, max = 20)
    private String managerSecond;
}
