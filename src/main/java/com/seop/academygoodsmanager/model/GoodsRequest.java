package com.seop.academygoodsmanager.model;

import com.seop.academygoodsmanager.enums.GoodsName;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class GoodsRequest {
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private GoodsName goodsName;
    @NotNull
    private Double goodsPrice;
    @NotNull
    private Integer managerNum;
    @NotNull
    @Length(min = 2, max = 20)
    private String receivePeople;
}
