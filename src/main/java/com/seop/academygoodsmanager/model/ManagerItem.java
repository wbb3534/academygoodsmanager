package com.seop.academygoodsmanager.model;

import com.seop.academygoodsmanager.entity.Manager;
import com.seop.academygoodsmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ManagerItem {
    private Long id;
    private Long goodsId;
    private String goodsName;
    private String managerTag;
    private LocalDate receiveDate;
    private String managerName;
    private String isUsed;

    private ManagerItem(ManagerItemBuilder builder) {
        this.id = builder.id;
        this.goodsId = builder.goodsId;
        this.goodsName = builder.goodsName;
        this.managerTag = builder.managerTag;
        this.receiveDate = builder.receiveDate;
        this.managerName = builder.managerName;
        this.isUsed = builder.isUsed;
    }
    public static class ManagerItemBuilder implements CommonModelBuilder<ManagerItem> {
        private final Long id;
        private final Long goodsId;
        private final String goodsName;
        private final String managerTag;
        private final LocalDate receiveDate;
        private final String managerName;
        private final String isUsed;

        public ManagerItemBuilder(Manager manager) {
            this.id = manager.getManagerId();
            this.goodsId = manager.getGoods().getId();
            this.goodsName = manager.getGoods().getGoodsName().getGoodsName();
            this.managerTag = manager.getClassName().getName() + "-" + manager.getGoods().getManagerNum();
            this.receiveDate = manager.getGoods().getReceiveDate();
            this.managerName = "정 " + manager.getManagerFirst() + "/" + "부 " + manager.getManagerSecond();
            this.isUsed = manager.getIsUsed() ? "Y" : "N";

        }
        @Override
        public ManagerItem build() {
            return new ManagerItem(this);
        }
    }
}
