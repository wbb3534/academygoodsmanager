package com.seop.academygoodsmanager.repository;

import com.seop.academygoodsmanager.entity.Manager;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface ManagerRepository extends JpaRepository<Manager, Long> {
    List<Manager> findAllByGoods_ReceiveDateOrderByManagerIdDesc(LocalDate searchDate);

    List<Manager> findAllByIsUsedOrderByManagerIdDesc(Boolean isUsed);
}
