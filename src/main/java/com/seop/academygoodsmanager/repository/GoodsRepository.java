package com.seop.academygoodsmanager.repository;

import com.seop.academygoodsmanager.entity.Goods;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoodsRepository extends JpaRepository<Goods, Long> {
}
