package com.seop.academygoodsmanager.service;

import com.seop.academygoodsmanager.entity.Goods;
import com.seop.academygoodsmanager.entity.Manager;
import com.seop.academygoodsmanager.exception.CMissingDataException;
import com.seop.academygoodsmanager.model.*;
import com.seop.academygoodsmanager.repository.ManagerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ManagerService {
    private final ManagerRepository managerRepository;

    public void setManager(Goods goods, ManagerRequest request) {
        Manager addData = new Manager.ManagerBuilder(goods, request).build();

        managerRepository.save(addData);
    }

    public ListResult<ManagerItem> getManagers() {
        List<Manager> originList = managerRepository.findAll();
        List<ManagerItem> result = new LinkedList<>();

       /* for (Manager item : originList) {
            ManagerItem addItem = new ManagerItem.ManagerItemBuilder(item).build();

            result.add(addItem);
        }*/
        originList.forEach(item -> result.add(new ManagerItem.ManagerItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }
    public ListResult<ManagerItem> getManagersDate(LocalDate searchDate) {
        List<Manager> originList = managerRepository.findAllByGoods_ReceiveDateOrderByManagerIdDesc(searchDate);
        List<ManagerItem> result = new LinkedList<>();

        /*for (Manager item : originList) {
            ManagerItem addItem = new ManagerItem.ManagerItemBuilder(item).build();

            result.add(addItem);
        }*/
        originList.forEach(item -> result.add(new ManagerItem.ManagerItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }
    public ListResult<ManagerItem> getManagersNoUse() {
        List<Manager> originList = managerRepository.findAllByIsUsedOrderByManagerIdDesc(false);
        List<ManagerItem> result = new LinkedList<>();

        /*for (Manager item : originList) {
            ManagerItem addItem = new ManagerItem.ManagerItemBuilder(item).build();

            result.add(addItem);
        }*/
        originList.forEach(item -> result.add(new ManagerItem.ManagerItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ManagerResponse getManger(long id) {
        Manager originData = managerRepository.findById(id).orElseThrow(CMissingDataException::new); //무조건 new붙은걸로

        return new ManagerResponse.ManagerResponseBuilder(originData).build();
    }

    public void putManagerName(long id, ManagerNameUpdateRequest request) {
        Manager originData = managerRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putManagerName(request);

        managerRepository.save(originData);
    }

    public void putPlaceIsUsed(long id, PlaceIsUsedUpdateRequest request) {
        Manager originData = managerRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putPlaceIsUsed(request);

        managerRepository.save(originData);
    }

    public void delManager(long id) {
        managerRepository.deleteById(id);
    }
}
