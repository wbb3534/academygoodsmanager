package com.seop.academygoodsmanager.service;

import com.seop.academygoodsmanager.entity.Goods;
import com.seop.academygoodsmanager.model.GoodsRequest;
import com.seop.academygoodsmanager.model.ManagerNumUpdateRequest;
import com.seop.academygoodsmanager.model.ManagerResponse;
import com.seop.academygoodsmanager.repository.GoodsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GoodsService {
    private final GoodsRepository goodsRepository;

    public void setGoods(GoodsRequest request) {
        Goods addData = new Goods.GoodsBuilder(request).build();

        goodsRepository.save(addData);
    }

    public Goods getGoodsId(long id) {
        return goodsRepository.findById(id).orElseThrow();
    }

    public void putManagerNum(long id, ManagerNumUpdateRequest request) {
        Goods originData = goodsRepository.findById(id).orElseThrow();

        originData.putManagerNum(request);

        goodsRepository.save(originData);
    }
}
