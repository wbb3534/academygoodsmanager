package com.seop.academygoodsmanager.controller;

import com.seop.academygoodsmanager.model.CommonResult;
import com.seop.academygoodsmanager.model.GoodsRequest;
import com.seop.academygoodsmanager.model.ManagerNumUpdateRequest;
import com.seop.academygoodsmanager.service.GoodsService;
import com.seop.academygoodsmanager.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/goods")
public class GoodsController {
    private final GoodsService goodsService;

    @PostMapping("/data")
    public CommonResult setGoods(@RequestBody @Valid GoodsRequest request) {
        goodsService.setGoods(request);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/manager-num/{id}")
    public CommonResult putManagerNum(@PathVariable long id, @RequestBody @Valid ManagerNumUpdateRequest request) {
        goodsService.putManagerNum(id, request);

        return ResponseService.getSuccessResult();
    }
}
