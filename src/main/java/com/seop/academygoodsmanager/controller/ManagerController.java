package com.seop.academygoodsmanager.controller;

import com.seop.academygoodsmanager.entity.Goods;
import com.seop.academygoodsmanager.model.*;
import com.seop.academygoodsmanager.service.GoodsService;
import com.seop.academygoodsmanager.service.ManagerService;
import com.seop.academygoodsmanager.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/manager")
public class ManagerController {
    private final GoodsService goodsService;
    private final ManagerService managerService;

    @PostMapping("/new/goods/{goodsId}")
    public CommonResult setManager(@PathVariable long goodsId, @RequestBody @Valid ManagerRequest request) {
        Goods goods = goodsService.getGoodsId(goodsId);
        managerService.setManager(goods, request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/list")
    public ListResult<ManagerItem> getManagers() {
        return ResponseService.getListResult(managerService.getManagers(), true);
    }
    @GetMapping("/list/date")
    public ListResult<ManagerItem> getManagersDate(@RequestParam("searchDate")@DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate searchDate) {
        return ResponseService.getListResult(managerService.getManagersDate(searchDate), true);
    }

    @GetMapping("/list/no-use")
    public ListResult<ManagerItem> getManagersNoUsed() {
        return ResponseService.getListResult(managerService.getManagersNoUse(), true);
    }

    @GetMapping("/detail/{id}")
    public SingleResult<ManagerResponse> getManager(@PathVariable long id) {
        return ResponseService.getSingleResult(managerService.getManger(id));
    }

    @PutMapping("/manager-name/{id}")
    public CommonResult putManagerName(@PathVariable long id, @RequestBody @Valid ManagerNameUpdateRequest request) {
        managerService.putManagerName(id, request);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/place-is-used/{id}")
    public CommonResult putPlaceIsUsed(@PathVariable long id, @RequestBody @Valid PlaceIsUsedUpdateRequest request) {
        managerService.putPlaceIsUsed(id, request);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/{id}")
    public CommonResult delManager(@PathVariable long id) {
        managerService.delManager(id);
        return ResponseService.getSuccessResult();
    }
}
