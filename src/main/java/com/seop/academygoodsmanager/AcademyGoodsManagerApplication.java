package com.seop.academygoodsmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcademyGoodsManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcademyGoodsManagerApplication.class, args);
	}

}
